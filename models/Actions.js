var mongoose = require('mongoose');
var Schema = mongoose.Schema;
if (mongoose.connection.readyState === 0) {
    mongoose.connect(require('./connection-string'));
}


//Guest model schema.
var newSchema = new Schema({
    'clientId':{type:String},
    'actionName': { type: String},
    'actionUrl': { type: String},
    'actionHeaders': { type: String},
    'actionData': { type: String},
    'actionMethod': { type: String},
    'actionSlotFields': { type: Object, default: {}},
    'actionResponse': { type: Array ,default:[]},
    'createdAt': { type: Date, default: Date.now },
    'updatedAt': { type: Date, default: Date.now }
});

newSchema.pre('save', function (next) {
    this.updatedAt = Date.now();
    next();
});

newSchema.pre('update', function () {
    this.update({}, { $set: { updatedAt: Date.now() } });
});

newSchema.pre('findOneAndUpdate', function () {
    this.update({}, { $set: { updatedAt: Date.now() } });
});


module.exports = mongoose.model('Actions', newSchema);
