var mongoose = require('mongoose');
var Schema = mongoose.Schema;
if (mongoose.connection.readyState === 0) {
    mongoose.connect(require('./connection-string'));
}

//hument  agent model schema.
var newSchema = new Schema({
    clientId: {
        type: String,
        required: true
    },
    agents: Array

},{ timestamps : true})



module.exports = mongoose.model('humanagents', newSchema);
