var mongoose = require('mongoose');
var Schema = mongoose.Schema;
if (mongoose.connection.readyState === 0) {
  mongoose.connect(require('./connection-string'));
}


var newSchema = new Schema({
  'userId': { type: String },
  'session':{type:Object},
  'context':{type:Object},
  'formdata':{type:Object,default:{}},
  'leadStatus':{type:Number, default:0},  //2 for archieveList & 1 For leadList(when buisness closed) & 0 for totalList 
  'clientId':{
    type: Schema.ObjectId,
    ref: 'Client'
  },
  'chatControl':{type:Number, default:0},
  'msgCounter':{type:Number,default:1},
  'callStatus':{type:Number,default:0},
  'node':{type:String},
  'phone':{type:String,default:null},
  'email':{type:String,default:null},
  'location':{type:Object},
  'name':{type:String},
  'fileUploaded':{type:Array,default:[]},
  'userStatus':{type:Number, default:0}, //for status i.e Buisness closed, In progress etc..
  'online':{type:Number, default:0},
  'status':{type:Number,default:0},  /*Verify=1 or not verify=0*/
  'chats':{type:Array,default:[]},
  'date': {type:Date, default: Date.now },
  'agentCurrentConrol':{
    "agentId" : {
      type : String,
      default: ""
    },
    "currentControl": {
      type : Boolean,
      default : false
    }
},
  'agentCommunicated':{
    type : Array ,
    default: []
  },
  'createdAt': {type:Date, default: Date.now },
  'updatedAt': {type:Date, default: Date.now }
});

newSchema.pre('save', function(next){
  this.updatedAt = Date.now();
  next();
});

newSchema.pre('update', function() {
  this.update({}, { $set: { updatedAt: Date.now() } });
});

newSchema.pre('findOneAndUpdate', function() {
  this.update({}, { $set: { updatedAt: Date.now() } });
});



module.exports = mongoose.model('Userchat', newSchema);
