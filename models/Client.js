var mongoose = require('mongoose');
var Schema = mongoose.Schema;
if (mongoose.connection.readyState === 0) {
  mongoose.connect(require('./connection-string'));
}


var newSchema = new Schema({
  'clientId': { type: String },
  'name':{type:String},
  'host': { type: String },
  'email': { type: String },
  'phone': { type: Number},
  'accepted_trmscndtn': {type: String, default: 'no'},
  'email_verified': {type: Boolean, default: false}, //To verify email while registeration true means verified
  'email_token': {type: String}, //Token to validate link sent for email verification
  'category': { type: String },
  'status': { type: String },
  'type':{ type: String },
  'story':{type:String,default:"True"},
  'open':{type:String,default:"False"},
  'userToken':{ type: String },
  'deviceId':{ type: String },
  'devType':{ type: String },
  'password':{type:String},
  'blockStatus':{type:String,default:0},
  'otp':{type:Number},
  'profile_img':{type:String},
  'timezone':{type: String, default:"Asia/Kolkata"},
  'location':{type:Object},
 //0 for website and 1 for Yubo App 
  'token':{type:String},
  'reset_token':{type:String},//for forgot password
  "tree":{type:Object},
  "btree":{type:Object},  //Branching tree
  "renderData":{type:Object},
  "custom_design": {type: Boolean, default: false},
  "isActionEnabled": {type: Boolean, default: false},
  'createdAt': { type: Date, default: Date.now },
  'updatedAt': { type: Date, default: Date.now }
});

newSchema.pre('save', function(next){
  this.updatedAt = Date.now();
  next();
});

newSchema.pre('update', function() {
  this.update({}, { $set: { updatedAt: Date.now() } });
});

newSchema.pre('findOneAndUpdate', function() {
  this.update({}, { $set: { updatedAt: Date.now() } });
});



module.exports = mongoose.model('Client', newSchema);
