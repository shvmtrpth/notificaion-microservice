var mongoose = require('mongoose');
var Schema = mongoose.Schema;
if (mongoose.connection.readyState === 0) {
    mongoose.connect(require('./connection-string'));
}


//Guest model schema.
var newSchema = new Schema({
    'clientId':{type:String},
    'url': { type: String},
    'host': { type: String},
    'count': {type: Number, default: 1},
    'createdAt': { type: Date, default: Date.now },
    'updatedAt': { type: Date, default: Date.now }
});

newSchema.pre('save', function (next) {
    this.updatedAt = Date.now();
    next();
});

newSchema.pre('update', function () {
    this.update({}, { $set: { updatedAt: Date.now(), count: count + 1 } });
});

newSchema.pre('findOneAndUpdate', function () {
    this.update({}, { $set: { updatedAt: Date.now(), count: count + 1 } });
});


module.exports = mongoose.model('Logs', newSchema);
