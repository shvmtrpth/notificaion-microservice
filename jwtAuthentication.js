const jsonwebtoken = require('jsonwebtoken');
const exceptions = require('./exception');

function verify(token){
    
    var decoded = jsonwebtoken.verify(token, process.env.SecretKey,(err,decoded)=>{
        if(err){
            throw exceptions('INVALID TOKEN');
        }else {
            return decoded;
        }
    });
}


function secure(req,res,next){
    try{
        var tok = req.headers.token;  
        if(!tok){
            throw exceptions('TOKEN_NOT_FOUND');
        }
        var security = verify(tok); 
      next();
    }
    catch(error){
        console.log('securebecinnnn',error)
        next(error)
    }
    
}

module.exports = {
    secure,
}