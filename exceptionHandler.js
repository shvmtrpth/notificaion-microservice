"use strict";

/**
 *@description Error Handler
 */
exports.errorHandler = (err, req, res, next) => {
  console.log('shivammmm!!!',err)
  return res.status(err.status || 500).json({
    status: false,
    statusMessage: err.message,
    statusCode: err.statusCode || 'unexpectedError',
    response: err.response || {},
  });
};


/**
 *@description Unknown Route Handler
 */
exports.unknownRouteHandler = (req, res) => {
  return res.status(400).json({
    status: false,
    statusMessage: '404 - Page Not found',
    statusCode: 'routeNotFound',
    response: {},
  });
};

