require('dotenv').config();
 const express= require('express');
 const bodyParser = require('body-parser');
 const router = require('./routes/index');
 const app = express();
 const exceptions  = require('./exceptionHandler')

 app.use(bodyParser.json());
 app.use(bodyParser.urlencoded(true))
 app.listen('3001',()=>{
    console.log('server started on 3001');
})
router(app);
app.use(exceptions.errorHandler);



