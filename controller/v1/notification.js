const axios = require('axios');
const db = require("../../models/all-models");
async function notification(req,res,next){
try{
if((req.body.phone && req.body.msg)){

   let clientId = "docgenie";
   let optinApi =  await axios.get('https://media.smsgupshup.com/GatewayAPI/rest', {
        params: {
           userid       : parseInt(process.env.userId),
           password     : process.env.password,
           phone_number : req.body.phone,
           method       : "OPT_IN",
           auth_scheme  : 'plain',
           channel      : 'whatsapp',
           format       : 'json',
           v            : 1.1
        }
    })

    if(optinApi.data.response.status == 'success'){
        /** please replace clientId with dynamic clienId */

      const data = await db.Userchat.findOne({"userId":{$regex : `wa_${req.body.phone}`},"clientId" : "5e5613e142388319d28d4b13"},{chats: { $slice: -1 },});
   
         if(!data){
            let response = await axios.get('https://media.smsgupshup.com/GatewayAPI/rest', {
              params: {
               userid       : parseInt(process.env.userId),
               password     : process.env.password,
               send_to      : req.body.phone,
               method       : 'SendMessage',
               auth_scheme  : 'plain',
               channel      : 'whatsapp',
               format       : 'json',
               msg          : req.body.msg,
               v            : 1.1,
               isHSM        : true,
               msg_type     : 'HSM',
               isTemplate   : true
            }
            });
            return res.json({
                statusCode: 200,
                status: true,
                message: "Notification sent Succesfully"
            });
           }
           const dbDate = new Date(data.chats[0].createdAt).getTime();
           const currentDate =  new Date().getTime();

         if((currentDate-dbDate)>=86400000){
             
            let response = await axios.get('https://media.smsgupshup.com/GatewayAPI/rest', {
                params: {
                 userid       : parseInt(process.env.userId),
                 password     : process.env.password,
                 send_to      : req.body.phone,
                 method       : 'SendMessage',
                 auth_scheme  : 'plain',
                 channel      : 'whatsapp',
                 format       : 'json',
                 msg          : req.body.msg,
                 v            : 1.1,
                 isHSM        : true,
                 msg_type     : 'HSM',
                 isTemplate   : true
              }
              });
          }else{
            
            let response = await axios.get('https://media.smsgupshup.com/GatewayAPI/rest', {
                params: {
                 userid       : parseInt(process.env.userId),
                 password     : process.env.password,
                 send_to      : req.body.phone,
                 method       : 'SendMessage',
                 auth_scheme  : 'plain',
                 channel      : 'whatsapp',
                 format       : 'json',
                 msg          : req.body.msg,
                 v            : 1.1,
                 isHSM        : false,
                 msg_type     : 'DATA_TXT',
                 isTemplate   : true
              }
              });
          }
          return res.json({
            statusCode: 200,
            status: true,
            message: "Notification sent Succesfully"
        });

    }else{
        return res.json({
            statusCode: 201,
            status: false,
            message: response.data.response.details
        });
        }
    } else {
        return res.json({
            statusCode: 201,
            status: false,
            message: "Phone and msg key is mandatory"
        });
        }           

    } catch(error){
        next(error);
    }
   
}

module.exports = {
    notification
}
