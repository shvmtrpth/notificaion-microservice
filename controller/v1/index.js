const controller = require('./notification');
const express = require('express');
const Router = express.Router();
const jwt = require('../../jwtAuthentication')
Router.post('/',jwt.secure,controller.notification);
module.exports =  Router